# Capture Traffic

The objective of the project is to capture the http traffic and log it, as long as a proxy is enabled in the browser.

## Get Started

To run the project you'll need to type this into the terminal
```
$ git clone https://lucart@bitbucket.org/lucart/capture-traffic.git
$ cd capture-traffic
$ node capture-traffic -p {Port when you want to start the proxy}
```
Example:
```
$ node capture-traffic -p 8213
```
Now you need to enable the proxy setting on your web browser or your network configuration.

Log files are automatically generated in the directory /logs