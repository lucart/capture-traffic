var http = require('http'),
    net = require('net'),
    httpProxy = require('http-proxy'),
    url = require('url'),
    log4js = require('log4js'),
    parseArgs = require('minimist')(process.argv.slice(2));

log4js.configure({
  appenders: {
    out:{ type: 'console' },
    app:{ type: 'file', filename: 'logs/traffic.log' }
  },
  categories: {
    default: { appenders: [ 'out', 'app' ], level: 'debug' }
  }
});

var logger = log4js.getLogger();
var proxy = httpProxy.createServer();
var serverPort = parseArgs['p'];

logger.info('Starting proxy server on port ' + serverPort);

proxy.on('proxyRes', function (proxyRes, req, res) {
  logger.info('Response: ' + JSON.stringify(proxyRes.headers, true, 2));
});

var server = http.createServer(function (req, res) {
  logger.info('Request: ' + JSON.stringify(req.headers, true, 2));

  proxy.web(req, res, {target: req.url, secure: false});
}).on("error", err=>logger.error(err['code']))
  .listen(serverPort);

server.on('connect', function (req, socket) {
  var serverUrl = url.parse('http://' + req.url);

  var srvSocket = net.connect(serverUrl.port, serverUrl.hostname, function() {
    socket.write('HTTP/1.1 200 Connection Established\r\n' +
    'Proxy-agent: Node-Proxy\r\n' +
    '\r\n');
    srvSocket.pipe(socket);
    socket.pipe(srvSocket);
  });
});